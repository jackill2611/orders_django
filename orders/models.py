from django.db import models
import uuid


class Product(models.Model):

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=200, null=False)

    def __str__(self):
        return self.name


class OrderItem(models.Model):

    ord_line_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    order_line_line_no = models.IntegerField(default=0)
    order_id = models.ForeignKey('Order', on_delete=models.CASCADE)
    product_id = models.ForeignKey('Product', on_delete=models.DO_NOTHING)
    product_price = models.DecimalField(max_digits=15, decimal_places=2, null=False)
    amount = models.IntegerField(default=0)

    @classmethod
    def create(cls, order_line_line_no, order_id, product_id, product_price, amount):
        ordritem = cls(ord_line_id=str(uuid.uuid4().hex)[0:36], order_line_line_no=order_line_line_no,
                       order_id=order_id, product_id=product_id, product_price=product_price, amount=amount)
        return ordritem

    def __str__(self):
        return str(self.order_line_line_no)+' '+str(self.product_id)


class Order(models.Model):

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    created_date = models.DateTimeField(null=False)
    number = models.CharField(max_length=10, null=True)
    order_sum = models.DecimalField(max_digits=15, decimal_places=2, null=True)

    @classmethod
    def create(cls, number, created_date):
        order = cls(number=number, created_date=created_date)
        return order

    def __str__(self):
        return str(self.number) + ' '+str(self.created_date)

    def get_pk_value(self):
        return self.id


