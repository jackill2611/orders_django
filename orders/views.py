from django.http import HttpResponse
from django.shortcuts import render
from .models import Order, OrderItem
from django.db.models import Sum
from django.db.models import Q
from django.db import connection
from django.db import reset_queries



def index(request):
    return HttpResponse("Orders index.")


def report1(request):
    reset_queries()
    if request.method == 'POST':
        startdate = request.POST.get('startdate')
        enddate = request.POST.get('enddate')
        print(startdate)
        print(str(startdate)+'  '+str(enddate))
        if startdate > enddate:
            error = 'Incorrect interval: start date should be equal or greater than end date!'
            return render(request, 'report1.html', {'error': error})
        else:
            reportdata = Order.objects.filter(created_date__range=[startdate, enddate])
            # prefetch linked orderitems
            orderlines = OrderItem.objects.prefetch_related('order_id')
            print (orderlines)
            data_array = []
            report_total = 0

            for ord in reportdata:
                order_sum = 0
                fetched_orderlines = orderlines.filter(order_id=ord)
                for ord_item in fetched_orderlines:
                    order_sum += ord_item.product_price*ord_item.amount
                    ord_item.order_sum = order_sum
                    report_total += ord_item.product_price*ord_item.amount
                data_array.append([ord, fetched_orderlines, order_sum])

            return render(request, 'report1.html', {'startdate': startdate, 'enddate': enddate,
                                                    'reportdata': data_array, 'report_total': report_total, 'query_info':connection.queries})
    else:
        return render(request, 'report1.html')


def report2(request):
    reset_queries()
    # aggregate sum of quantity of each product
    itemsales = OrderItem.objects.values('product_id').annotate(amount_total=Sum('amount')).order_by('-amount_total')
    name_map = {'product_id': 'product_id_id', 'ord_line_id': 'ord_line_id', 'pk': 'ord_line_id', 'total_amount':'total_amount'}
    rawquery = OrderItem.objects.raw('''SELECT
        orderitems.ord_line_id as ord_line_id,
        orderitems.product_id_id as product_id_id,
        Sums.total_amount,
        orderinfo.order_num as order_num,
        orderinfo.created_date as order_created_date       
        FROM orders_OrderItem as orderitems
        LEFT JOIN(SELECT product_id_id,
        SUM(amount) as total_amount 
        FROM orders_OrderItem
        GROUP BY product_id_id
        ORDER BY -SUM(amount)) as Sums on orderitems.product_id_id = Sums.product_id_id
        LEFT JOIN(SELECT id, created_date, "number" as order_num
        FROM orders_Order) as orderinfo on orderitems.order_id_id = orderinfo.id        
        ORDER BY -Sums.total_amount
        LIMIT 100
    ''', translations=name_map)
    data_array = []
    for p in rawquery:
        data_array.append([p.order_id.number, p.order_id.created_date, p.product_id, p.product_price])

    return render(request, 'report2.html', {'reportdata':data_array, 'query_info':connection.queries})

