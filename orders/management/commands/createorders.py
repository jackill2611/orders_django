from django.core.management.base import BaseCommand, CommandError
import random
import datetime
import pytz
from ...models import Product, Order, OrderItem


class Command(BaseCommand):
    help = 'Creates orders, first argument means quantity of orders to be created'

    def add_arguments(self, parser):
        parser.add_argument('orders_qty', nargs='+', type=int)

    def handle(self, *args, **options):
        # Create [orders_qty] orders
        generated_orders = []
        start_date = datetime.datetime(2018, 1, 1, tzinfo=pytz.UTC)
        qty_of_products = Product.objects.count()
        if qty_of_products == 0:
            warning_text = 'can\'c generate orders because there are no products in a base!'
            raise CommandError(warning_text)
        print('qty of products in this base: '+str(qty_of_products))
        products_dict = Product.objects.all()
        self.stdout.write('Creating '+str(options['orders_qty'][0])+' orders')
        for n in range(1, options['orders_qty'][0]+1):
            ordr = Order.create(n, start_date + datetime.timedelta(days=n-1))
            ordr.save()
            generated_orders.append(ordr)
            # generate random 1..5 order lines
            random.seed()
            no_of_orderlines = random.randint(1, 5)
            for lineno in range(1, no_of_orderlines+1):
                random_product = products_dict[random.randint(0, qty_of_products-1)]
                ordrline = OrderItem.create(lineno, ordr, random_product, random.randint(100, 999), random.randint(1, 10))
                ordrline.save()

        self.stdout.write(self.style.SUCCESS('Successfully completed a command, created "%s" orders'
                                             % str(len(generated_orders))))
