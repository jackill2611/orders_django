from django.contrib import admin

# the module name is app_name.models
from .models import Product, OrderItem, Order

# Register your models to admin site, then you can add, edit, delete and search your models in Django admin site.
admin.site.register(Product)
admin.site.register(OrderItem)
admin.site.register(Order)
