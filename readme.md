# README #

This README is about steps that are necessary to get this application up and running.


### How do I get set up? ###

* Install Docker (docker-compose (https://docs.docker.com/compose/install/)

* Clone the project from this repository to your local folder.

* (if you are Mac/Linux user) change permissions of all .sh files inside to x:  chmod +x *.sh

* Run terminal (Term1) from this folder

        docker-compose up -d
        
* Make sure that all containers are up and running:

        docker container list -a

* To use custom admin command to create some test data use (from orders_django folder):

        docker-compose run app /usr/local/bin/python manage.py createorders <noOfOrders>
        
* Go to 127.0.0.1:8000        

* If you want to use django-admin, go to 127.0.0.1:8000/admin    (login: admin, password: test)

* to use Report1 , go to 127.0.0.1:8000/orders/report1

* to use Report2 , go to 127.0.0.1:8000/orders/report2        
